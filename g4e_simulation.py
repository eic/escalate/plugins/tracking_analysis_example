from g4epy import Geant4Eic

g4e = Geant4Eic(beamline='erhic')

# Set beam energies
g4e.command(['/detsetup/eBeam 10',
             '/detsetup/pBeam 100'
             ])

# Extension is ommited here
# g4e creates a bunch of files with this name and different extensions
g4e.output('OUTPUTS/g4e_mstruct_general')

# Run g4e run!!!
g4e.source('/home/romanov/Downloads/pipi.lund') \
   .beam_on(1000) \
   .run()