# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
#
from pyjano.jana import Jana, PluginFromSource

mstruct_general = PluginFromSource('./mstruct_general')   # Name will be determined from folder name
                                                          # add name=<...> for custom name

jana = Jana(nevents=1000, output='OUTPUTS/mstruct_gen.root')
jana.plugin('lund_reader')\
    .source('/home/romanov/Downloads/pipi.lund')

# Parameters:
#     verbose   - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     smearing  - Particle smearing 0-true MC, 1-smearing, 2-reconstruction");
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(mstruct_general, verbose=1)

jana.run()
