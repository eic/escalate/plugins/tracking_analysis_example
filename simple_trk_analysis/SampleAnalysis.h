
#ifndef EJANA_SampleAnalysis_H
#define EJANA_SampleAnalysis_H

#include <TH1F.h>
#include <TH3F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TDatabasePDG.h>

#include <vector>

#include <JANA/JEventProcessor.h>
#include <ejana/EServicePool.h>
#include <JANA/Services/JGlobalRootLock.h>



class JEvent;
class JApplication;

class SampleAnalysis:public JEventProcessor
{
public:
    explicit SampleAnalysis(JApplication *);
    ~SampleAnalysis() override = default;

    //----------------------------
    // Init
    //
    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;

    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

private:
    TDirectory* dir_main;               /// Main TDirectory for this plugin
    ej::EServicePool services;          /// <Temporary> service locator
    
    std::shared_ptr<JGlobalRootLock> m_lock;
    
    Int_t pdgCode;
	Int_t motherID;
    Double_t x_gen, y_gen, z_gen, r_gen;
	Double_t px_gen, py_gen, pz_gen, pt_gen, p_gen;
	Double_t theta_gen, phi_gen, eta_gen, rap_gen;
	Double_t x_rec, y_rec, z_rec, r_rec;
	Double_t px_rec, py_rec, pz_rec, pt_rec, p_rec;
	Double_t theta_rec, phi_rec, eta_rec, rap_rec;
	Double_t m_gen, m_rec;
	Double_t E_gen, E_rec;
	
	//Double_t firstHitX, firstHitY;
	//Double_t lastHitX, lastHitY;
	//
	//std::vector<Double_t> xHits;
	//std::vector<Double_t> yHits;
	//std::vector<Double_t> zHits;
	
	//Need to make arrays, for the TTree
	//Double_t * xHitsArray;
	//Double_t * yHitsArray;
	//Double_t * zHitsArray;
	
	Double_t diffX, diffY, diffZ; //Difference in vertex position for simulation and reconstruction
	Double_t diffR;
	Double_t diffPx, diffPy, diffPz, diffPt, diffP; //Difference in momenta between simulation and reconstruction
	Double_t diffTheta, diffPhi, diffEta, diffRap; //Difference in the "angles" between simulation and reconstruction
    
    
    TTree * analysis = new TTree("analysis","analysisStuff");
    
    TBranch * bpdgCode;
    TBranch * bmotherID;
    
    TBranch * bx_gen;
    TBranch * by_gen;
    TBranch * bz_gen;
    TBranch * br_gen;
    TBranch * bpx_gen;
    TBranch * bpy_gen;
    TBranch * bpz_gen;
    TBranch * bpt_gen;
    TBranch * bp_gen;
    TBranch * btheta_gen;
    TBranch * bphi_gen; 
    TBranch * beta_gen;
    TBranch * brap_gen;
    
    TBranch * bm_gen;
    TBranch * bE_gen;
    
    
    TBranch * bx_rec;
    TBranch * by_rec;
    TBranch * bz_rec;
    TBranch * br_rec;
    TBranch * bpx_rec;
    TBranch * bpy_rec;
    TBranch * bpz_rec;
    TBranch * bpt_rec;
    TBranch * bp_rec;
    TBranch * btheta_rec;
    TBranch * bphi_rec;
    TBranch * beta_rec; 
    TBranch * brap_rec; 
    
    TBranch * bm_rec;
    TBranch * bE_rec;
    
    //TBranch * bfirstHitX;
    //TBranch * bfirstHitY;
    //TBranch * blastHitX;
    //TBranch * blastHitY;
    //
    //TBranch * bxHits;
    //TBranch * byHits;
    //TBranch * bzHits;
    
    TBranch * bdiffX;
    TBranch * bdiffY;
    TBranch * bdiffZ;
    
    TBranch * bdiffR;
    
    TBranch * bdiffPx;
    TBranch * bdiffPy;
    TBranch * bdiffPz;
    TBranch * bdiffPt;
    TBranch * bdiffP;
    
    TBranch * bdiffTheta;
    TBranch * bdiffPhi;
    TBranch * bdiffEta;
    TBranch * bdiffRap;
    
    
    
    //TBranch * bFullParticleTest;
    
    
    struct ParticleStruct {
		Double_t x_gen, y_gen, z_gen, r_gen;
		Double_t px_gen, py_gen, pz_gen, pt_gen, p_gen;
		
	};
    
    
    //If we want to only look at the primary particles
    bool onlyPrimaryParticles = true;
    //Note: unused, for now.
    
    
    TDatabasePDG * pdgDatabase = new TDatabasePDG();
    
    TFile * file;
    
};

#endif //EJANA_SampleAnalysis_H
