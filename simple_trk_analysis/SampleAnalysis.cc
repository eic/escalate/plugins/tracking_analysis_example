#include <iostream>
#include <algorithm>

#include <TDirectory.h>
#include <TCanvas.h>
#include <TPad.h>

#include <JANA/JApplication.h>
#include <JANA/JEvent.h>

#include <fmt/core.h>

#include <ejana/EStringHelpers.h>
#include <ejana/EServicePool.h>

#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McFluxHit.h>




#include <MinimalistModel/McTrack.h>

#include <MinimalistModel/McGeneratedVertex.h>
#include "RecoModel/RecoHit.h"
#include "RecoModel/RecoTrackCand.h"
#include "RecoModel/RecoTrack.h"
#include "RecoModel/RecoVertex.h"


#include "SampleAnalysis.h"


using namespace fmt;

//------------------
// SampleAnalysis (Constructor)
//------------------
SampleAnalysis::SampleAnalysis(JApplication *app) :
	JEventProcessor(app),
	services(app)
{
}

//------------------
// Init
//------------------
void SampleAnalysis::Init()
{
	// Ask service locator a file to write to
	file = services.Get<TFile>();
	//Gets us the file that we create as the JANA output file, in the running command. (.plugin('jana', output='hello_processed.root'))
	
	// Root related, we switch gDirectory to this file
	file->cd();
	gDirectory->pwd();
	
	
	//Add all the branches we want (all the variables of interest) to our new tree
	
	bpdgCode = analysis->Branch("pdgCode",&pdgCode,"pdgCode/I");
	bmotherID = analysis->Branch("motherID",&motherID,"motherID/I");
	//If no mother, we know that it's an original track.
	
	bm_gen = analysis->Branch("m_gen",&m_gen,"m_gen/D");
	bx_gen = analysis->Branch("x_gen",&x_gen,"x_gen/D");
	by_gen = analysis->Branch("y_gen",&y_gen,"y_gen/D");
	bz_gen = analysis->Branch("z_gen",&z_gen,"z_gen/D");
	br_gen = analysis->Branch("r_gen",&r_gen,"r_gen/D");
	bpx_gen = analysis->Branch("px_gen",&px_gen,"px_gen/D");
	bpy_gen = analysis->Branch("py_gen",&py_gen,"py_gen/D");
	bpz_gen = analysis->Branch("pz_gen",&pz_gen,"pz_gen/D");
	bpt_gen = analysis->Branch("pt_gen",&pt_gen,"pt_gen/D");
	bp_gen = analysis->Branch("p_gen",&p_gen,"p_gen/D");
	bE_gen = analysis->Branch("E_gen",&E_gen,"E_gen/D");
	btheta_gen = analysis->Branch("theta_gen",&theta_gen,"theta_gen/D");
	bphi_gen = analysis->Branch("phi_gen",&phi_gen,"phi_gen/D");
	beta_gen = analysis->Branch("eta_gen",&eta_gen,"eta_gen/D");
	brap_gen = analysis->Branch("rap_gen",&rap_gen,"rap_gen/D");
	
	bm_rec = analysis->Branch("m_rec",&m_rec,"m_rec/D");
	bx_rec = analysis->Branch("x_rec",&x_rec,"x_rec/D");
	by_rec = analysis->Branch("y_rec",&y_rec,"y_rec/D");
	bz_rec = analysis->Branch("z_rec",&z_rec,"z_rec/D");
	br_rec = analysis->Branch("r_rec",&r_rec,"r_rec/D");
	bpx_rec = analysis->Branch("px_rec",&px_rec,"px_rec/D");
	bpy_rec = analysis->Branch("py_rec",&py_rec,"py_rec/D");
	bpz_rec = analysis->Branch("pz_rec",&pz_rec,"pz_rec/D");
	bpt_rec = analysis->Branch("pt_rec",&pt_rec,"pt_rec/D");
	bp_rec = analysis->Branch("p_rec",&p_rec,"p_rec/D");
	bE_rec = analysis->Branch("E_rec",&E_rec,"E_rec/D");
	btheta_rec = analysis->Branch("theta_rec",&theta_rec,"theta_rec/D");
	bphi_rec = analysis->Branch("phi_rec",&phi_rec,"phi_rec/D");
	beta_rec = analysis->Branch("eta_rec",&eta_rec,"eta_rec/D");
	brap_rec = analysis->Branch("rap_rec",&rap_rec,"rap_rec/D");
	
	//bfirstHitX = analysis->Branch("firstHitX",&firstHitX,"firstHitX/D");
	//bfirstHitY = analysis->Branch("firstHitY",&firstHitY,"firstHitY/D");
	//
	//blastHitX = analysis->Branch("lastHitX",&lastHitX,"lastHitX/D");
	//blastHitY = analysis->Branch("lastHitY",&lastHitY,"lastHitY/D");
	//
	//
	//
	//bxHits = analysis->Branch("xHits",&xHitsArray,"xHitsArray[8]/D");
	//byHits = analysis->Branch("yHits",&yHitsArray,"yHitsArray[8]/D");
    //bzHits = analysis->Branch("zHits",&zHitsArray,"zHitsArray[8]/D");
    
	
	//Calculated things. b because we want the variable name for the variables
	bdiffX = analysis->Branch("diffX", &diffX, "diffX/D");
	bdiffY = analysis->Branch("diffY", &diffY, "diffY/D");
	bdiffZ = analysis->Branch("diffZ", &diffZ, "diffZ/D");
	
	bdiffR = analysis->Branch("diffR", &diffR, "diffR/D");
	
	bdiffPx = analysis->Branch("diffPx", &diffPx, "diffPx/D");
	bdiffPy = analysis->Branch("diffPy", &diffPy, "diffPy/D");
	bdiffPz = analysis->Branch("diffPz", &diffPz, "diffPz/D");
	bdiffPt = analysis->Branch("diffPt", &diffPt, "diffPt/D");
	bdiffP = analysis->Branch("diffP", &diffP, "diffP/D");
	
	bdiffTheta = analysis->Branch("diffTheta", &diffTheta, "diffTheta/D");
	bdiffPhi = analysis->Branch("diffPhi", &diffPhi, "diffPhi/D");
	bdiffEta = analysis->Branch("diffEta", &diffEta, "diffEta/D");
	bdiffRap = analysis->Branch("diffRap", &diffRap, "diffRap/D");
	
	// Create a directory for this plugin. And subdirectories for series of histograms
	//dir_main = file->mkdir("sample_ana");
	
}


//------------------
// Process
//------------------
//This happens for each event.
void SampleAnalysis::Process(const std::shared_ptr<const JEvent>& event)
{
    fmt::print("SampleAnalysis::Process() called. Event number: {}\n", event->GetEventNumber());
	
	auto hits = event->Get<RecoHit>();
    //auto reco_tracks = event->Get<RecoTrack>("acts");
    auto reco_tracks = event->Get<RecoTrack>("genfit");
    auto mc_tracks = event->Get<minimodel::McTrack>();
	
	auto clusters = event->Get<RecoTrackCand>();
	
	auto mc_generated_vertices = event->Get<minimodel::McGeneratedVertex>();
    auto reco_vertices = event->Get<RecoVertex>();
        
    
    //Create maps on track ID.
    //hardly necessary for simple one-two particle events, but for more advanced ones it will be good.
    std::map<uint64_t, const minimodel::McTrack *> mcTracksMap;
    std::map<uint64_t, const RecoTrack *> recoTracksMap;
    for (auto &mc_track : mc_tracks) {
        assert(mcTracksMap.find(mc_track->id) == mcTracksMap.end()); //Just check so we don't make doubles
        mcTracksMap[mc_track->id] = mc_track; //Sets the track ID to map to this one.
	}
	for (auto &reco_track : reco_tracks) {
        assert(recoTracksMap.find(reco_track->track_id) == recoTracksMap.end()); //Just check so we don't make doubles
        recoTracksMap[reco_track->track_id] = reco_track; //Sets the track ID to map to this one.
	}

	
	//Let's loop through the MC map.
	//And for each track, check if there is a reonstructed one. Then add to tree if so.
	//For all:
	for (auto mc_track : mcTracksMap) {
		//Check if there isn't a reco_track with the same ID. If there isn't, just skip this one for now
		if (recoTracksMap.find(mc_track.first) == recoTracksMap.end()) {
			continue;
		}
		
		const RecoTrack * reco_track = recoTracksMap[mc_track.first];
		
		pdgCode = mc_track.second->pdg;
		
		
		//For all:
		motherID = mc_track.second->parent_id;
		
		pdgCode = mc_track.second->pdg;
		m_gen = (pdgDatabase->GetParticle(pdgCode))->Mass(); //Gives mass in GeV. https://root.cern.ch/doc/master/TParticlePDG_8h_source.html#l00026
		m_rec = m_gen; //Assuming ideal PID for now.
		
		
		x_gen = mc_track.second->vtx_x;
		y_gen = mc_track.second->vtx_y;
		z_gen = mc_track.second->vtx_z;
		r_gen = TMath::Sqrt(x_gen*x_gen + y_gen*y_gen + z_gen*z_gen);
		
		px_gen = mc_track.second->px;
		py_gen = mc_track.second->py;
		pz_gen = mc_track.second->pz;
		pt_gen = TMath::Sqrt(px_gen*px_gen + py_gen*py_gen);
		p_gen = mc_track.second->p;
		E_gen = TMath::Sqrt(m_gen*m_gen + p_gen*p_gen); //In GeV
		
		theta_gen = TMath::ACos(mc_track.second->dir_z);
		phi_gen = TMath::ATan2(mc_track.second->dir_x, mc_track.second->dir_y);  
		eta_gen = -1.0*TMath::Log(TMath::Tan(theta_gen/2.0));
		rap_gen = 0.5*TMath::Log((E_gen + pz_gen)/(E_gen - pz_gen)); //y = \frac{1}{2} \ln{\left( \frac{E + p_\text{L} c}{E - p_\text{L} c} \right)}
		
		
		//Reconstructed
		x_rec = reco_track->x[0];
		y_rec = reco_track->x[1];
		z_rec = reco_track->x[2];
		r_rec = reco_track->x.Mag();
		
		px_rec = reco_track->p[0];
		py_rec = reco_track->p[1];
		pz_rec = reco_track->p[2];
		pt_rec = reco_track->p.Pt();
		p_rec = reco_track->p.Mag();
		E_rec = TMath::Sqrt(m_rec*m_rec + p_rec*p_rec); //In GeV
		
		theta_rec = reco_track->theta;
		phi_rec = TMath::ATan2(px_rec, py_rec);   
		eta_rec = -1.0*TMath::Log(TMath::Tan(theta_rec/2.0));
		rap_rec = 0.5*TMath::Log((E_rec + pz_rec)/(E_rec - pz_rec));
		
		//Difference
		diffX = x_rec - x_gen;
		diffY = y_rec - y_gen;
		diffZ = z_rec - z_gen;
		
		diffR = r_rec - r_gen;
		
		diffPx = px_rec - px_gen;
		diffPy = py_rec - py_gen;
		diffPz = pz_rec - pz_gen;
		diffPt = pt_rec - pt_gen;
		diffP = p_rec - p_gen;
		
		diffTheta = theta_rec - theta_gen;
		diffPhi = phi_rec - phi_gen;
		diffEta = eta_rec - eta_gen;
		diffRap = rap_rec - rap_gen;
		
		analysis->Fill();
		
    } //For MC tracks
	
}


//------------------
// Finish
//------------------
void SampleAnalysis::Finish()
{
	
	//file->Write();
	
	fmt::print("SampleAnalysis::Finish() called\n");
}


